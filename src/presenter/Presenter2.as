package presenter
{
	import event.CustomEvent;

	public class Presenter2
	{
		[Mediate(event="CustomEvent.EVENT3")]
		public function catchEvent3():void
		{
			trace("Event 3 caught in Presenter 2")
		}
	}
}