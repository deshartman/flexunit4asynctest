package presenter
{
	import controller.Controller1;
	
	import event.CustomEvent;
	
	import flash.events.IEventDispatcher;
	
	public class Presenter1
	{
		[Dispatcher] public var eventDispatcher:IEventDispatcher;
		
		public function sendEvent1():void
		{
			trace("Dispatching Event 1 from Presenter 1");
			eventDispatcher.dispatchEvent(new CustomEvent(CustomEvent.EVENT1));
		}
	}
}