package controller
{
	import event.CustomEvent;
	
	import flash.events.IEventDispatcher;

	public class Controller2
	{
		[Dispatcher] public var eventDispatcher:IEventDispatcher;
		
		[Mediate(event="CustomEvent.EVENT2")]
		public function catchEvent2():void
		{
			trace("Event 2 caught in Controller 2")

			trace("Dispatching event 3 from Controller 2");
			eventDispatcher.dispatchEvent(new CustomEvent(CustomEvent.EVENT3));
		}
	}
}