package controller
{
	import event.CustomEvent;
	
	import flash.events.IEventDispatcher;

	public class Controller1
	{
		[Dispatcher] public var eventDispatcher:IEventDispatcher;
		
		[Mediate(event="CustomEvent.EVENT1")]
		public function catchEvent1():void
		{
			trace("Event 1 caught in Controller 1")
			
			trace("Dispatching event 2 from Controller 1");
			eventDispatcher.dispatchEvent(new CustomEvent(CustomEvent.EVENT2));
		}
	}
}