package test
{
	import controller.Controller1;
	import controller.Controller2;
	
	import event.CustomEvent;
	
	import flexunit.framework.Assert;
	
	import org.flexunit.async.Async;
	import org.swizframework.core.SwizConfig;
	import org.swizframework.core.mxml.Swiz;
	
	import presenter.Presenter1;
	import presenter.Presenter2;

	public class TestFlow
	{		
		private var swizConfig:SwizConfig;
		private var swiz:Swiz;
		
		private var presenter1:Presenter1;
		private var presenter2:Presenter2;
		private var controller1:Controller1;
		private var controller2:Controller2;
		
		[Before]
		public function runBeforeEveryTest():void
		{
			swiz = new Swiz();
			swizConfig = new SwizConfig;
			
			swizConfig.strict = true;
			swizConfig.viewPackages = "view.*";
			swizConfig.eventPackages = "event.*";
			
			// Read the config and initialise Swiz
			swiz = new Swiz(null,swizConfig,null,[Beans]);
			swiz.init();
			
			// Get a reference to the Object created by Swiz
			presenter1 = ((swiz.beanProviders[0] as Beans).presenter1 as Presenter1);
			presenter2 = ((swiz.beanProviders[0] as Beans).presenter2 as Presenter2);
			
			controller1 = ((swiz.beanProviders[0] as Beans).controller1 as Controller1);
			controller2 = ((swiz.beanProviders[0] as Beans).controller2 as Controller2);
	
		}
		
		[After]
		public function runAfterEveryTest():void
		{
			
		}
		
		[Test(async)]
		public function test1():void
		{
			Async.proceedOnEvent(this, presenter1.eventDispatcher, CustomEvent.EVENT1, 500, noEvent);
			Async.proceedOnEvent(this, controller1.eventDispatcher, CustomEvent.EVENT2, 500, noEvent);
			Async.proceedOnEvent(this, controller2.eventDispatcher, CustomEvent.EVENT3, 500, noEvent);
			
			presenter1.sendEvent1();
		}
		
		
		
		private function noEvent(passthough:Object):void
		{
			Assert.fail(">>>>>>>  No event fired <<<<<<<");
		}
		
	}
}