package event
{
	import flash.events.Event;
	
	public class CustomEvent extends Event
	{
		public static const EVENT1:String = "Event1";
		public static const EVENT2:String = "Event2";
		public static const EVENT3:String = "Event3";
		public static const EVENT4:String = "Event4";
		
		
		public function CustomEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}